from time import time
import numpy as np
import baseball
import income
import matplotlib.pyplot as plt
import sys

from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.datasets import load_digits
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
from sklearn import preprocessing

np.random.seed(42)
frequency_table=False

sample_size = 1000

data, target_classes, feature_names, target_names = income.get_data(sample_size)

data = scale(data)

n_samples, n_features = data.shape
n_income = len(np.unique(target_classes))
labels = target_classes

def show_classifications(k, target_classes, pred_classes):
    target_names = ['<=50K', '>50K']
    income_encoder = preprocessing.LabelEncoder()
    income_encoder.fit(target_names)

    cluster_table = {}
    for i in range(k):
        freq_table = {}
        for j in range(2):
            freq_table[j] = 0

        cluster_table[i] = freq_table

    for i in range(len(pred_classes)):
        cluster = pred_classes[i]
        pitch = target_classes[i]
        cluster_table[cluster][pitch] += 1

    for i in range(k):
        output_dict = {}
        cluster_total = sum(cluster_table[i].values())
        for k, v in cluster_table[i].items():
            output_dict[list(income_encoder.inverse_transform([k]))[0]] = v/cluster_total * 100

        sorted_by_value = sorted(output_dict.items(), key=lambda kv: kv[1], reverse=True)

        print('{}: '.format(i))
        for item in sorted_by_value:
            print('\t%s: %.2f' % (item[0], item[1]))

print("n_income: %d, \t n_samples %d, \t n_features %d"
      % (n_income, n_samples, n_features))


print('init\t\ttime\tinertia\thomo\tcompl\tv-meas\tARI\tAMI\tsilho\tFMI\tDBI\tCHI')
print(100 * '-')

for k in range(10,11):
# for k in range(2,3):
    kmeans = KMeans(init='k-means++', n_clusters=k, n_init=10)
    t0 = time()
    kmeans.fit(data)
    pred_classes = kmeans.predict(data)
    name = str(k) + ' clusters'
    print('%-9s\t%.2fs\t%i\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f'
          % (name, (time() - t0), kmeans.inertia_,
             metrics.homogeneity_score(labels, kmeans.labels_),
             metrics.completeness_score(labels, kmeans.labels_),
             metrics.v_measure_score(labels, kmeans.labels_),
             metrics.adjusted_rand_score(labels, kmeans.labels_),
             metrics.adjusted_mutual_info_score(labels,  kmeans.labels_,average_method='arithmetic'),
             metrics.silhouette_score(data, kmeans.labels_,
                                      metric='euclidean',
                                      sample_size=sample_size),
             metrics.fowlkes_mallows_score(labels, kmeans.labels_),
             metrics.davies_bouldin_score(data, kmeans.labels_),
             metrics.calinski_harabaz_score(data, kmeans.labels_)))

    if frequency_table:
        show_classifications(k,target_classes, pred_classes)

print(100 * '-')