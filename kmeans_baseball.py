from time import time
import numpy as np
import baseball
import dimensional_reduction
import income
import matplotlib.pyplot as plt
import sys

from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.datasets import load_digits
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
from sklearn import preprocessing

dr = True
print_classes = False
plot_diagrams = True

np.random.seed(42)
sample_size = 1000
data, target_classes, feature_names, target_names = baseball.get_data(sample_size)
scaled_data = scale(data)

n_samples, n_features = data.shape
n_pitches = len(np.unique(target_classes))
labels = target_classes

def show_classifications(k, target_classes, pred_classes):
    target_names = ['CH', 'CU', 'EP', 'FC', 'FF', 'FO', 'PO', 'FS', 'FT', 'IN', 'KC', 'KN', 'SC', 'SI', 'SL', 'UN']
    pitch_type_encoder = preprocessing.LabelEncoder()
    pitch_type_encoder.fit(target_names)

    cluster_table = {}
    for i in range(k):
        freq_table = {}
        for j in range(15):
            freq_table[j] = 0

        cluster_table[i] = freq_table

    for i in range(len(pred_classes)):
        cluster = pred_classes[i]
        pitch = target_classes[i]
        cluster_table[cluster][pitch] += 1

    for i in range(k):
        output_dict = {}
        cluster_total = sum(cluster_table[i].values())
        for k, v in cluster_table[i].items():
            output_dict[list(pitch_type_encoder.inverse_transform([k]))[0]] = v/cluster_total * 100

        sorted_by_value = sorted(output_dict.items(), key=lambda kv: kv[1], reverse=True)

        print('{}: '.format(i))
        for item in sorted_by_value:
            print('\t%s: %.2f' % (item[0], item[1]))

print("n_pitches: %d, \t n_samples %d, \t n_features %d"
      % (n_pitches, n_samples, n_features))


print('init\t\ttime\tinertia\thomo\tcompl\tv-meas\tARI\tAMI\tsilho\tFMI\tDBI\tCHI')
print(100 * '-')

for k in range(5,6):
# for k in range(5,6):
    X = scaled_data.copy()

    if dr:
        X = dimensional_reduction.Pca(X, 2)
        # X = dimensional_reduction.Ica(X, 2)
        # X = dimensional_reduction.Rp(X, 2)
        # X = dimensional_reduction.Lda(X, target_classes, 2)

    kmeans = KMeans(init='k-means++', n_clusters=k, n_init=10)
    t0 = time()
    kmeans.fit(X)
    kmeans_pred_classes = kmeans.predict(X)
    name = str(k) + ' clusters'
    print('%-9s\t%.2fs\t%i\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f'
          % ('kmeans ' + name, (time() - t0), kmeans.inertia_,
             metrics.homogeneity_score(labels, kmeans.labels_),
             metrics.completeness_score(labels, kmeans.labels_),
             metrics.v_measure_score(labels, kmeans.labels_),
             metrics.adjusted_rand_score(labels, kmeans.labels_),
             metrics.adjusted_mutual_info_score(labels,  kmeans.labels_,average_method='arithmetic'),
             metrics.silhouette_score(X, kmeans.labels_,
                                      metric='euclidean',
                                      sample_size=sample_size),
             metrics.fowlkes_mallows_score(labels, kmeans.labels_),
             metrics.davies_bouldin_score(X, kmeans.labels_),
             metrics.calinski_harabaz_score(X, kmeans.labels_)))

    if print_classes:
        show_classifications(k,target_classes, kmeans_pred_classes)

    if plot_diagrams:
        # Step size of the mesh. Decrease to increase the quality of the VQ.
        h = .02     # point in the mesh [x_min, x_max]x[y_min, y_max].

        # Plot the decision boundary. For that, we will assign a color to each
        x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
        y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

        # Obtain labels for each point in mesh. Use last trained model.
        Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])

        # Put the result into a color plot
        Z = Z.reshape(xx.shape)
        plt.figure(1)
        plt.clf()
        plt.imshow(Z, interpolation='nearest',
                extent=(xx.min(), xx.max(), yy.min(), yy.max()),
                cmap=plt.cm.Paired,
                aspect='auto', origin='lower')

        plt.plot(X[:, 0], X[:, 1], 'k.', markersize=2)
        # Plot the centroids as a white X
        centroids = kmeans.cluster_centers_
        plt.scatter(centroids[:, 0], centroids[:, 1],
                    marker='x', s=169, linewidths=3,
                    color='w', zorder=10)
        plt.title('K-means clustering on the digits dataset (PCA-reduced data)\n'
                'Centroids are marked with white cross')
        plt.xlim(x_min, x_max)
        plt.ylim(y_min, y_max)
        plt.xticks(())
        plt.yticks(())
        plt.show()

print(100 * '-')