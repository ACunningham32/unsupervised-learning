
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
import baseball
import income
from sklearn.preprocessing import scale
from sklearn import decomposition
from sklearn import datasets
from sklearn import random_projection
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

np.random.seed(42)

def Pca(X, n_components):
    pca = decomposition.PCA(n_components=n_components)
    pca.fit(X)
    X_pca = pca.transform(X)
    return X_pca

def Pca_stats():
    sample_size = 1000
    data, target_classes, feature_names, target_names = baseball.get_data(sample_size)
    scaled_data = scale(data)
    y = target_classes
    n_features = len(feature_names)

    covar_matrix = decomposition.PCA(n_components = n_features) #we have 33 features
    covar_matrix.fit(scaled_data)
    X_pca = covar_matrix.transform(scaled_data)
    print(X_pca.shape)
    variance = covar_matrix.explained_variance_ratio_ #calculate variance ratios

    var=np.cumsum(np.round(variance, decimals=3)*100)
    var #cumulative sum of variance explained with [n] features
    print(var)

    plt.ylabel('% Variance Explained')
    plt.xlabel('# of Features')
    plt.title('PCA Analysis')
    plt.ylim(20,100.5)
    plt.style.context('seaborn-whitegrid')
    plt.plot(var)
    plt.show()


def Ica(X, n_components):
    ica = decomposition.FastICA(n_components=n_components, max_iter=1000)
    X_ica = ica.fit_transform(X)
    return X_ica

def Ica_stats():
    sample_size = 1000
    data, target_classes, feature_names, target_names = baseball.get_data(sample_size)
    scaled_data = scale(data)
    y = target_classes
    n_features = len(feature_names)

    ica = decomposition.FastICA(n_components = n_features, max_iter=10000) #we have 33 features
    tmp = ica.fit_transform(scaled_data)
    tmp = pd.DataFrame(tmp)
    tmp = tmp.kurt(axis=0)
    kurt = tmp.abs().mean()
    print(kurt)

    # plt.ylabel('% Variance Explained')
    # plt.xlabel('# of Features')
    # plt.title('PCA Analysis')
    # plt.ylim(20,100.5)
    # plt.style.context('seaborn-whitegrid')
    # plt.plot(var)
    # plt.show()

def Rp(X, n_components):
    rp = random_projection.SparseRandomProjection(n_components=n_components)
    X_rp = rp.fit_transform(X)
    return X_rp

def Lda(X, y, n_components):
    lda = LinearDiscriminantAnalysis(n_components=n_components)
    X_lda = lda.fit(X, y).transform(X)
    return X_lda


if __name__ == "__main__":
    # Ica_stats()
    Pca_stats()