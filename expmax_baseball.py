import matplotlib as mpl
import matplotlib.pyplot as plt

import numpy as np
import sys
import baseball
import income
import time
import warnings

from sklearn import datasets
from sklearn.mixture import GaussianMixture
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import scale
from itertools import cycle, islice

sample_size = 1000
data, target_classes, feature_names, target_names = baseball.get_data(sample_size)

# normalize dataset for easier parameter selection
X = StandardScaler().fit_transform(data)

for k in range(2, 17):
    algorithm = GaussianMixture(n_components=k, covariance_type='full')
    t0 = time.time()

    # catch warnings related to kneighbors_graph
    with warnings.catch_warnings():
        warnings.filterwarnings(
            "ignore",
            message="the number of connected components of the " +
            "connectivity matrix is [0-9]{1,2}" +
            " > 1. Completing it to avoid stopping the tree early.",
            category=UserWarning)
        warnings.filterwarnings(
            "ignore",
            message="Graph is not fully connected, spectral embedding" +
            " may not work as expected.",
            category=UserWarning)
        algorithm.fit(X)

    t1 = time.time()
    if hasattr(algorithm, 'labels_'):
        y_pred = algorithm.labels_.astype(np.int)
    else:
        y_pred = algorithm.predict(X)

    # colors = np.array(list(islice(cycle(['#377eb8', '#ff7f00', '#4daf4a',
    #                                         '#f781bf', '#a65628', '#984ea3',
    #                                         '#999999', '#e41a1c', '#dede00']),
    #                                 int(max(y_pred) + 1))))
    colors = np.array(list(islice(cycle(['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231',
                                         '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe',
                                         '#008080', '#e6beff', '#9a6324', '#fffac8', '#800000',
                                         '#aaffc3', '#808000', '#ffd8b1', '#000075', '#808080',
                                         '#ffffff', '#000000']),
                                    int(max(y_pred) + 1))))
    # add black color for outliers (if any)
    colors = np.append(colors, ["#000000"])
    plt.scatter(X[:, 0], X[:, 1], s=10, color=colors[y_pred])

    plt.xlim(-2, 2)
    plt.ylim(-2, 2)
    plt.xticks(())
    plt.yticks(())
    plt.text(.99, .01, ('%.2fs' % (t1 - t0)).lstrip('0'),
                transform=plt.gca().transAxes, size=15,
                horizontalalignment='right')
    # plot_num += 1
    plt.show()