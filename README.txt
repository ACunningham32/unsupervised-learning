README.txt

Neural Network:
Find my repo at: https://bitbucket.org/ACunningham32/unsupervised-learning/src/master/

1. git clone

2. pip install -r requirements.txt

Run PCA dimensionality reduction:

    python dimensional_reduction.py

Run K-means clustering:

    python kmeans_baseball.py
    python kmeans_income.py

Run Expectation Maximization clustering:

    python expmax_baseball.py

Run GMM selection:

    python gmm_selection.py

Set sample size with sample_size parameter at the top of each file

To Run Chad's code with my data and changes:

    cd chad/unsupervised-learning

    pip install -r requirements.txt

    python run_experiment.py --all