import numpy as np
from sklearn import random_projection
import baseball

sample_size = 1000
data, target_classes, feature_names, target_names = baseball.get_data(sample_size)

X = np.array(data)
print(X.shape)
transformer = random_projection.SparseRandomProjection(n_components=8)
transformer = random_projection.GaussianRandomProjection(n_components=8)
X_new = transformer.fit_transform(X)
print(X_new.shape)